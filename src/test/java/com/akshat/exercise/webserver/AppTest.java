package com.akshat.exercise.webserver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  public static void main(final String[] args) {

    final Pattern HeaderLine = Pattern.compile("(.*):(.*)");
    final String str = "Host: localhost:8089";
    final String[] parts = str.split(":", 2);
    System.out.println(parts[0] + " <-------->" + parts[1]);
    final Matcher matcher = HeaderLine.matcher(str);
    if (matcher.matches()) {
      System.out.println(matcher.group(0) + " <-------->" + matcher.group(1));
    }

  }
  /**
   * Create the test case
   *
   * @param testName
   *          name of the test case
   */
  public AppTest(final String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(AppTest.class);
    }

  /**
   * Rigourous Test :-)
   */
  public void testApp() {
    assertTrue(true);
  }
}
