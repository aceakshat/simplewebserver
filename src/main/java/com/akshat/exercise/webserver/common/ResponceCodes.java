package com.akshat.exercise.webserver.common;

public enum ResponceCodes {
  _200("200 OK"), _400("400 Bad Request"), _403("403 Forbidden"), _404("404 Not Found"), _500("Internal Server Error");

  private String code;
  ResponceCodes(final String code) {
    this.code = code;
  }

  public String getCode() {
    return this.code;
  }
}
