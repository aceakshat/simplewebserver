package com.akshat.exercise.webserver;

import java.net.ServerSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.task.TaskExecutor;

import com.akshat.exercise.webserver.service.ConfigService;
import com.akshat.exercise.webserver.service.RequestHandler;

/**
 * Simple web server main class.
 *
 */
@SpringBootApplication
public class App implements CommandLineRunner, ApplicationContextAware {
  public static final Logger LOGGER = LoggerFactory.getLogger(App.class);

  @Autowired
  private TaskExecutor executor;

  @Autowired
  private ConfigService config;

  private ApplicationContext ctx;

  public static void main(final String[] args) {
    SpringApplication.run(App.class);
  }

  @Override
  public void run(final String... args) throws Exception {
    LOGGER.info("Server is listning on port:{}", this.config.getPort());
    LOGGER.info("Server serving from root home directory: {}", this.config.getHomeDir());
    try (final ServerSocket serverSocket = new ServerSocket(this.config.getPort());) {
      for (;;) {
        final RequestHandler requestHandler = (RequestHandler) this.ctx.getBean("requestHandler");
        requestHandler.setClientSocket(serverSocket.accept());
        this.executor.execute(requestHandler);
      }
    }
  }

  @Override
  public void setApplicationContext(final ApplicationContext ctx) throws BeansException {
    this.ctx = ctx;
  }
}
