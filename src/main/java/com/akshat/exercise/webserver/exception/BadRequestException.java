package com.akshat.exercise.webserver.exception;

import com.akshat.exercise.webserver.common.ResponceCodes;

public class BadRequestException extends Exception {
  private static final long serialVersionUID = 1L;
  private final ResponceCodes errorCode;
  private String version;

  public BadRequestException(final ResponceCodes errorCode, final String version, final String message) {
    super(message);
    this.errorCode = errorCode;
    this.version = version;
  }

  public ResponceCodes getErrorCode() {
    return this.errorCode;
  }

  public String getVersion() {
    return this.version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }
}
