package com.akshat.exercise.webserver.model;

import static com.akshat.exercise.webserver.common.ResponceCodes._200;
import static com.akshat.exercise.webserver.common.ResponceCodes._400;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.io.File.separatorChar;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import com.akshat.exercise.webserver.common.ResponceCodes;
import com.akshat.exercise.webserver.exception.BadRequestException;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

public class Request {
  private String method;
  private String resource;
  private String version;
  private Map<String, String> headers;
  private boolean keepAlive;
  private String mimeType;
  private Long contentLength = 0l;

  public Long getContentLength() {
    return this.contentLength;
  }

  public void setContentLength(final Long contentLength) {
    this.contentLength = contentLength;
  }

  private ResponceCodes code;

  public static Request buildRequest(final BufferedReader reader) throws IOException, BadRequestException {
    final Request request = new Request();
    final String line = reader.readLine();// GET /index.html HTTP/1.1
    if (Strings.isNullOrEmpty(line)) {
      return null;
    }
    final String[] parts = line.split("\\s");
    if (parts.length != 3) {
      request.code = _400;
      throw new BadRequestException(_400, "HTTP/1.1", "Invalid request");
    }
    request.method = parts[0].trim();
    request.resource = parts[1].trim().replace('/', separatorChar); // Replace "/" with os based path separator
    request.version = parts[2].trim();
    request.headers = buildHeaders(reader);
    request.keepAlive = isKeepAlive(request);
    request.mimeType = getMimeType(request.resource);
    request.code = _200;
    return request;
  }

  public static Request buildRequest(final BadRequestException exception) {
    final Request request = new Request();
    request.code = exception.getErrorCode();
    request.version = exception.getVersion();
    return request;
  }

  private static Map<String, String> buildHeaders(final BufferedReader reader) throws IOException {
    final Map<String, String> headers = Maps.newHashMap();
    String line;
    while (!isNullOrEmpty(line = reader.readLine())) {
      final String parts[] = line.split(":", 2);
      if (parts.length == 2) {
        headers.put(parts[0].trim(), parts[1].trim());
      }
    }
    return headers;
  }

  private static boolean isKeepAlive(final Request request) {
    boolean keepAlive = false;
    if ("HTTP/1.1".equals(request.version)) {
      keepAlive = true;// true by default
      if (request.headers.containsKey("Connection") && request.headers.get("Connection").contains("close")) {
        keepAlive = false;
      }
    } else if ("HTTP/1.0".equals(request.version)) {
      keepAlive = false;// false by default
      if (request.headers.containsKey("Connection") && request.headers.get("Connection").contains("keep-alive")) {
        keepAlive = true;
      }
    }
    return keepAlive;
  }

  private static String getMimeType(final String resource) {
    String mimeType = "application/octet-stream";
    if (resource.endsWith(".html") || resource.endsWith(".htm")) {
      mimeType = "text/html";
    } else if (resource.endsWith(".jpg") || resource.endsWith(".jpeg")) {
      mimeType = "image/jpeg";
    } else if (resource.endsWith(".gif")) {
      mimeType = "image/gif";
    }
    return mimeType;
  }

  public String getMimeType() {
    return this.mimeType;
  }

  public boolean isKeepAlive() {
    return this.keepAlive;
  }

  public String getMethod() {
    return this.method;
  }

  public String getResource() {
    return this.resource;
  }

  public String getVersion() {
    return this.version;
  }

  public Map<String, String> getHeaders() {
    return this.headers;
  }

  public ResponceCodes getCode() {
    return this.code;
  }
}
