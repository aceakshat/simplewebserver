package com.akshat.exercise.webserver.model;

import java.util.Map;

import com.google.common.collect.Maps;

public class Response {
  private final Map<String, String> header = Maps.newHashMap();
  private String resultCode;
  private String resource;

  public void addHeaderValue(final String key, final String value) {
    this.header.put(key, value);
  }

  public String getResultHeader() {
    final StringBuilder headerStr = new StringBuilder();
    this.header.forEach((k, v) -> headerStr.append(k).append(":").append(v).append("\r\n"));
    return headerStr.toString();
  }

  public String getResource() {
    return this.resource;
  }

  public void setResource(final String resource) {
    this.resource = resource;
  }

  public String getResultCode() {
    return this.resultCode;
  }

  public void setResultCode(final String resultCode) {
    this.resultCode = resultCode;
  }

}
