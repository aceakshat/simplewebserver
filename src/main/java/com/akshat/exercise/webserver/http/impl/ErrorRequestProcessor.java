package com.akshat.exercise.webserver.http.impl;

import java.io.BufferedOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.akshat.exercise.webserver.common.Method;
import com.akshat.exercise.webserver.exception.BadRequestException;
import com.akshat.exercise.webserver.http.RequestProcessor;
import com.akshat.exercise.webserver.model.Request;
import com.akshat.exercise.webserver.model.Response;
@Component
@Scope("prototype")
public class ErrorRequestProcessor extends RequestProcessor {
  public static final Logger LOGGER = LoggerFactory.getLogger(ErrorRequestProcessor.class);

  @Override
  public Response processRequest(final Request request, final BufferedOutputStream outstream) throws BadRequestException, IOException {
    final String header = buildHeader(request, request.getCode());
    LOGGER.info("error header " + header);
    outstream.write(header.getBytes());
    outstream.flush();
    return new Response();
  }

  @Override
  public Method getMethod() {
    return Method.ERROR;
  }
}
