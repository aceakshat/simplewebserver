package com.akshat.exercise.webserver.http;

import static com.akshat.exercise.webserver.common.ResponceCodes._200;

import java.io.BufferedOutputStream;
import java.io.IOException;

import com.akshat.exercise.webserver.common.Method;
import com.akshat.exercise.webserver.common.ResponceCodes;
import com.akshat.exercise.webserver.exception.BadRequestException;
import com.akshat.exercise.webserver.model.Request;
import com.akshat.exercise.webserver.model.Response;

public abstract class RequestProcessor {

  /**
   * This Method processes the request based on the HTTP method.
   * 
   * @return
   */
  public abstract Response processRequest(Request request, BufferedOutputStream stream) throws BadRequestException, IOException;

  /**
   * This returns the HTTP method of the implementation.
   * 
   * @return
   */
  public abstract Method getMethod();

  protected String buildHeader(final Request request, final ResponceCodes code) {
    final StringBuilder headerStr = new StringBuilder();
    headerStr.append(request.getVersion()).append(" ").append(code.getCode()).append("\r\n");
    headerStr.append("Content-Length").append(":").append(request.getContentLength().toString()).append("\r\n");
    if (_200.equals(code)) {
      headerStr.append("Content-Type").append(":").append(request.getMimeType()).append("\r\n");
    }
    if (request.isKeepAlive()) {
      headerStr.append("Connection: keep-alive\r\n");
    } else {
      headerStr.append("Connection: close\r\n");
    }
    return headerStr.append("\r\n").toString();
  }
}
