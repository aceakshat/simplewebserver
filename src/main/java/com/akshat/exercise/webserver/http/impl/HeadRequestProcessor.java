package com.akshat.exercise.webserver.http.impl;

import static com.akshat.exercise.webserver.common.ResponceCodes._200;
import static com.akshat.exercise.webserver.common.ResponceCodes._404;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.akshat.exercise.webserver.common.Method;
import com.akshat.exercise.webserver.exception.BadRequestException;
import com.akshat.exercise.webserver.http.RequestProcessor;
import com.akshat.exercise.webserver.model.Request;
import com.akshat.exercise.webserver.model.Response;
import com.akshat.exercise.webserver.service.ConfigService;
@Component
@Scope("prototype")
public class HeadRequestProcessor extends RequestProcessor {
  public static final Logger LOGGER = LoggerFactory.getLogger(HeadRequestProcessor.class);
  @Autowired
  private ConfigService config;

  @Override
  public Response processRequest(final Request request, final BufferedOutputStream outstream) throws BadRequestException, IOException {
    final File fileToServe = new File(this.config.getHomeDir(), request.getResource());
    LOGGER.info("Request for serving file:" + fileToServe.getAbsolutePath());
    if (fileToServe.isFile()) {
      outstream.write(buildHeader(request, _200).getBytes());
      outstream.flush();
    } else {
      throw new BadRequestException(_404, request.getVersion(), request.getResource() + " is not a valid resource");
    }
    return new Response();
  }

  @Override
  public Method getMethod() {
    return Method.HEAD;
  }
}
