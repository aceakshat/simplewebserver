package com.akshat.exercise.webserver.service;

import static com.akshat.exercise.webserver.common.ResponceCodes._400;
import static com.akshat.exercise.webserver.common.ResponceCodes._403;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.akshat.exercise.webserver.exception.BadRequestException;
import com.akshat.exercise.webserver.http.RequestProcessor;
import com.akshat.exercise.webserver.model.Request;
@Component
@Scope("prototype")
public class RequestHandler implements DisposableBean, Runnable {
  public static final Logger LOGGER = LoggerFactory.getLogger(RequestHandler.class);
  private Socket clientSocket;

  @Autowired
  private ConfigService config;

  @Autowired
  private List<RequestProcessor> processorList;

  @Autowired
  private RequestProcessor errorRequestProcessor;

  @Override
  public void run() {
    boolean keepAlive = false;
    try (final BufferedReader reader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
        final BufferedOutputStream out = new BufferedOutputStream(this.clientSocket.getOutputStream());) {
      this.clientSocket.setSoTimeout(this.config.getSocketTimeout());
      do {
        try {
          final Request request = Request.buildRequest(reader);
          if (request == null) {
            return;
          }
          LOGGER.info("Processing request[{}]", request);
          keepAlive = request.isKeepAlive();
          this.clientSocket.setKeepAlive(request.isKeepAlive());
          validateRequest(request);
          final RequestProcessor processor = getProcessor(request);
          if (processor == null) {
            throw new BadRequestException(_400, request.getVersion(), "Unsupported method");
          } else {
            processor.processRequest(request, out);
          }
        } catch (final SocketTimeoutException e) {
          LOGGER.error("Socket time out, closing socket");
          closeSocket();
        } catch (final BadRequestException e) {
          LOGGER.warn("Error serving request ", e);
          final Request request = Request.buildRequest(e);
          this.errorRequestProcessor.processRequest(request, out);
        }
      } while (keepAlive && !this.clientSocket.isClosed());
      closeSocket();
    } catch (final Exception e) {
      LOGGER.error("Error serving request ", e);
    }
  }

  private RequestProcessor getProcessor(final Request request) {
    for(final RequestProcessor processor : this.processorList) {
      if(processor.getMethod().getHttpMethod().equals(request.getMethod())) {
        return processor;
      }
    }
    return null;
  }

  private boolean validateRequest(final Request request) throws BadRequestException {
    if (request.getResource().indexOf("..") >= 0) {
      throw new BadRequestException(_403, request.getVersion(), "Illegal characters in request");
    }
    return true;
  }

  private void closeSocket() throws IOException {
    if (!this.clientSocket.isClosed()) {
      this.clientSocket.close();
    }
  }

  @Override
  public void destroy() throws Exception {
    closeSocket();
  }

  public void setClientSocket(final Socket clientSocket) {
    this.clientSocket = clientSocket;
  }
}
