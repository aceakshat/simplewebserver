package com.akshat.exercise.webserver.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
@Service
public class ConfigService {
  @Value("${home.dir}")
  private String homeDir;
  @Value("${simpleserver.port}")
  private Integer port;
  @Value("${socket.timeout}")
  private Integer socketTimeout;

  public String getHomeDir() {
    return this.homeDir;
  }

  public Integer getPort() {
    return this.port;
  }

  public void setHomeDir(final String homeDir) {
    this.homeDir = homeDir;
  }

  public void setPort(final Integer port) {
    this.port = port;
  }

  public Integer getSocketTimeout() {
    return this.socketTimeout;
  }

  public void setSocketTimeout(final Integer socketTimeout) {
    this.socketTimeout = socketTimeout;
  }

}
