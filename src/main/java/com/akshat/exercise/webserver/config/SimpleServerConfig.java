package com.akshat.exercise.webserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.akshat.exercise.webserver.http.RequestProcessor;
import com.akshat.exercise.webserver.http.impl.ErrorRequestProcessor;
import com.akshat.exercise.webserver.http.impl.GetRequestProcessor;
import com.akshat.exercise.webserver.http.impl.HeadRequestProcessor;
import com.akshat.exercise.webserver.service.ConfigService;

@Configuration
public class SimpleServerConfig {

  @Bean
  public TaskExecutor taskExecutor() {
    final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(10);
    executor.setMaxPoolSize(50);
    executor.setQueueCapacity(500);
    executor.setThreadNamePrefix("simpleServer-");
    executor.initialize();
    return executor;
  }

  @Bean
  public ConfigService configService() {
    return new ConfigService();
  }

  @Bean
  public RequestProcessor getRequestProcessor() {
    return new GetRequestProcessor();
  }

  @Bean
  public RequestProcessor headRequestProcessor() {
    return new HeadRequestProcessor();
  }

  @Bean
  public RequestProcessor errorRequestProcessor() {
    return new ErrorRequestProcessor();
  }
}
