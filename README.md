simpleServer
========

simpleServer is a simple http server. 


1. simpleServer needs the following properties, all these can be specified in application.properties file:
  i.simpleserver.port= Port for this server example 8089
  ii. home.dir= Home directory for the resources to serve
  iii. socket.timeout= timeout for keepAlive connections. 

default values of the properties above are in src/java/resources

2. To Build deployable jar
use: mvn clean install
deployable jar will be target/simpleServer-<version>.jar

3. Running the app:
put the application.properties in the same directory as the jar. 
To run the app use:
java -jar simpleserver-0.0.1-SNAPSHOT.jar

